<?php
// $Id:$

/**
 * @file
 * Module's admin settings.
 */

use publicplan\wss\sodis\Service;

/**
 * Callback function for SODIS specific administration settings.
 *
 * @return array Renderable form.
 */
function wss_sodis_admin_settings($form_id, $form_state) {
  $settings = array();

  $settings[Service::SETTING__ACTIVATED] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate SODIS search service'),
    '#description' => '<strong>' .
        t('Deactivation will also disable the type-ahead functionality.') .
        '</strong>',
    '#default_value' => Service::getSetting(
        Service::SETTING__ACTIVATED),
    '#weight' => 10,
  );

  $settings[Service::SETTING__SEARCH_URI] = array(
    '#type' => 'textfield',
    '#title' => t('Search service address'),
    '#description' => 'Syntax: http://sodis.example.com/learnline3-rest/rest/' .
        '<code>%KEY</code>/search.json<br/>' .
        '(<code>%KEY</code> ' . t('will be replaced by the API-Key.') . ')',
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__SEARCH_URI),
    '#weight' => 20,
  );

  $settings[Service::SETTING__TYPEAHEAD_URI] = array(
    '#type' => 'textfield',
    '#title' => t('Type-ahead service address'),
    '#description' => 'Syntax: http://sodis.example.com/learnline3-rest/rest/' .
        '<code>%KEY</code>/type-ahead.json<br/>' .
        '(<code>%KEY</code> ' . t('will be replaced by the API-Key.') . ')',
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__TYPEAHEAD_URI),
    '#weight' => 30,
  );

  $settings[Service::SETTING__RESOURCE_URI] = array(
    '#type' => 'textfield',
    '#title' => t('Resource address'),
    '#description' => 'Syntax: http://sodis.example.com/learnline3-rest/rest/' .
        '<code>%KEY</code>/objects/%RESOURCE<br/>' .
        '(<code>%KEY</code> ' . t('will be replaced by the API-Key.') . ')<br/>' .
        '(<code>%RESOURCE</code> ' . t('will be replaced by the appropriate resource ID.') . ')',
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__RESOURCE_URI),
    '#weight' => 40,
  );

  $settings[Service::SETTING__RESOURCE_DOWNLOAD_URI] = array(
    '#type' => 'textfield',
    '#title' => t('Resource download address'),
    '#description' => 'Syntax: http://sodis.example.com/learnline3-rest/rest/' .
        '<code>%KEY</code>/objects/%RESOURCE/download<br/>' .
        '(<code>%KEY</code> ' . t('will be replaced by the API-Key.') . ')<br/>' .
        '(<code>%RESOURCE</code> ' . t('will be replaced by the appropriate resource ID.') . ')',
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__RESOURCE_DOWNLOAD_URI),
    '#weight' => 50,
  );

  $settings[Service::SETTING__FACETS_URI] = array(
    '#type' => 'textfield',
    '#title' => t('Base URI for requesting lists of facet values'),
    '#description' => 'Syntax: http://sodis.example.com/learnline3-rest/rest/' .
        '<code>%KEY</code>/facets/%FACET<br/>' .
        '(<code>%KEY</code> ' . t('will be replaced by the API-Key.') . ')<br/>' .
        '(<code>%FACET</code> ' . t('will be replaced by the appropriate facet.') . ')',
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__FACETS_URI),
    '#weight' => 60,
  );

  $settings[Service::SETTING__ACCESS_TOKEN] = array(
    '#type' => 'textfield',
    '#title' => t('API-Key'),
    '#description' => t('Please enter a valid access token.'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__ACCESS_TOKEN),
    '#weight' => 70,
  );

  $settings[Service::SETTING__THUMBS_DIR] = array(
    '#type' => 'textfield',
    '#title' => t('Thumbs folder'),
    '#description' => t('Path to default thumbnail images directory.'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__THUMBS_DIR),
    '#weight' => 80,
  );

  $settings[Service::SETTING__EDMOND_BASE_URL] = array(
    '#type' => 'textfield',
    '#title' => t('Edmond Basis-URL'),
    '#description' => t('Standard: "https://nrw.edupool.de/search?func=record&src=online&record=#EDMOND_ID#&standort=#STANDORT#&handle=#SESSION_ID#"'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
      Service::SETTING__EDMOND_BASE_URL),
    '#weight' => 84,
  );

  $settings[Service::SETTING__EDMOND_AUTH_URL] = array(
    '#type' => 'textfield',
    '#title' => t('Edmond Auth-URL (for LOGINEO)'),
    '#description' => t('Standard: "https://nrw.edupool.de/lib/auth?record=#EDMOND_ID#"'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
      Service::SETTING__EDMOND_AUTH_URL),
    '#weight' => 85,
  );

  $settings[Service::SETTING__EDMOND_ID_PARAM] = array(
    '#type' => 'textfield',
    '#title' => t('Edmond ID Parameter'),
    '#description' => t('Query Parameter, der aus der übermittelten Edmond URL ausgelesen werden soll.'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
      Service::SETTING__EDMOND_ID_PARAM),
    '#weight' => 86,
  );

  $settings[Service::SETTING__EDMOND_HANDLE_URL] = array(
    '#type' => 'textfield',
    '#title' => t('Edmond Session Handle Request URI'),
    '#description' => t('URL zur Abfrage des Edmond Session Handle.'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
      Service::SETTING__EDMOND_HANDLE_URL),
    '#weight' => 87,
  );

  $settings[Service::SETTING__GET_LATEST_COUNT] = array(
    '#type' => 'textfield',
    '#title' => t('Number of resources to display as latest material'),
    '#description' => t("Define how many resource links will be listed as latest on front page."),
    '#size' => 2,
    '#maxlength' => 3,
    '#default_value' => Service::getSetting(
        Service::SETTING__GET_LATEST_COUNT),
    '#rules' => array('numeric', 'length[1,3]'),
    '#weight' => 90,
  );

  $settings[Service::SETTING__GET_LATEST_SORTING] = array(
    '#type' => 'select',
    '#title' => t('Sorting criteria for latest resources'),
    '#description' => t('Key to sort resources by.'),
    '#required' => FALSE,
    '#options' => Service::getSortings(),
    '#empty_option' => t('standard (relevance)'),
    '#empty_value' => '',
    '#default_value' => Service::getSetting(
        Service::SETTING__GET_LATEST_SORTING),
    '#weight' => 95,
  );

  $settings[Service::SETTING__GET_LATEST_RESTRICTION] = array(
    '#type' => 'textfield',
    '#title' => t('Restriction for getting latest resources'),
    '#description' => t('This is necessary to work around the fact, that the API encounters an error on requests without any search term.'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__GET_LATEST_RESTRICTION),
    '#weight' => 99,
  );

  return system_settings_form($settings);
} //function

<?php // $id:$

/**
 * @file
 * This file contains the Request class interface.
 */

namespace publicplan\wss\sodis;

use publicplan\wss\base\SearchRequest;

class Request extends SearchRequest {
  /**
   * Define cache table for requests of this kind.
   */
  const CACHE = 'cache_wss_sodis';

  /**
   * Identifier for this request class.
   *
   * This identifier is used for some inherited generic methods and to determine
   * the corresponding service class.
   *
   * @var string
   */
  protected $service = Service::NAME;

  /**
   * Request method (eg. GET/POST/PUT).
   *
   * @var string
   */
  protected $method = self::METHOD__GET;

  /**
   * Stores parameters in case of calling $this->setParameters().
   *
   * @var array
   */
  protected $parameters = array();

  /**
   * Maximum number of redirects to follow.
   *
   * @var int
   */
  protected $maxRedirects = 10;

  /**
   * Optional cURL options.
   *
   * @var array
   */
  protected $curlOpts = array();

  /**
   * Set cURL option.
   *
   * @param int $opt
   *   CURL_OPT constant/key.
   * @param mixed $value
   *   Option value.
   *
   * @return \publicplan\wss\sodis\Request
   *   Returns itself.
   */
  public function setCurlOpt($opt, $value = NULL) {
    $this->curlOpts[$opt] = $value;
    return $this;
  }

  /**
   * Set multiple cURL options at once as associative array.
   *
   * @param array $opts
   *   Associative array with CURL_OPT keys as keys and corresponding values.
   *
   * @return \publicplan\wss\sodis\Request
   *   Returns itself.
   */
  public function setCurlOpts($opts) {
    $this->curlOpts = array_merge($this->curlOpts, $opts);
    return $this;
  }

  /**
   * Return an associative array of currently set cURL options.
   *
   * @return array
   *   Associative array of currently set cURL options.
   */
  public function getCurlOpts() {
    return $this->curlOpts;
  }

  /**
   * Extend default request options in case of edmond resources, to avoid following location headers.
   *
   * @return array
   */
  protected function getOptions() {
    $provider = filter_input(INPUT_GET, 'provider');
    $options = parent::getOptions();
    $curl_opts = $this->getCurlOpts();
    if ($provider === 'edmond') {
      $curl_opts[CURLOPT_FOLLOWLOCATION] = FALSE;
    }
    if (!empty($curl_opts)) {
      $options['curl_opts'] = $curl_opts;
    }

    return $options;
  }

  /**
   * Assign query parameters.
   *
   * @param array $parameters
   *   Request parameters.
   *
   * @return \publicplan\wss\sodis\Request
   *   Returns itself.
   */
  public function setParameters($parameters) {
    if (isset($parameters['license_filter']) && !empty($parameters['license_filter'])) {
      $parameters += array(
        'licensedTo[]' => $parameters['license_filter'],
      );
      unset($parameters['license_filter']);
    } else {
      $parameters += array(
        'licensedTo' => array('LVR', 'LWL'),
      );
    }
    if (isset($parameters['q'])) {
      $parameters['q'] = Service::init()->escapeTerm($parameters['q']);
    }
    $this->parameters = $parameters;
    // Finally format the whole query:
    $base_url = str_replace('%KEY',
      Service::getSetting(Service::SETTING__ACCESS_TOKEN),
      Service::getSetting(Service::SETTING__SEARCH_URI));

    return $this->setUrl($base_url . $this->formatQueryString());
  }

  /**
   * SODIS specific parameter mapping.
   *
   * @return string
   *   Returns request URL for given parameters.
   */
  public function formatQueryString() {
    $query_components = array();
    foreach ($this->parameters as $key => $value) {
      if (is_array($value)) {
        $value = \array_filter($value, array($this, 'filterEmptyStrings'));
      }
      if (empty($value)) {
        continue;
      }
      $query_components[] = $this->urlEncodeComponent($key, $value);
    }

    return '?' . implode('&', $query_components);
  }

  /**
   * Return the acutal parameters array.
   *
   * @return array
   *   The current parameters array.
   */
  public function getParameters() {
    return $this->parameters;
  }

  /**
   * Array filter callback removing empty string.
   *
   * @param mixed $value
   *   Input value.
   *
   * @return bool
   *   TRUE for non-empty values.
   */
  private function filterEmptyStrings($value) {
    return !empty($value);
  }
}

<?php
// $id:$

/**
 * @file
 * This file contains the TypeAheadRequest class definition.
 */

namespace publicplan\wss\sodis;

use publicplan\wss\base\SearchRequest;

class TypeAheadRequest extends SearchRequest {
  const CACHE = 'cache_wss_sodis_typeahead';

  protected $service = Service::NAME;
  protected $method = self::METHOD__GET;

  /**
   * This is where we store the decoded results.
   *
   * @var array
   */
  protected $results = array();

  /**
   * Completely configure the Request object by specifying the search term.
   *
   * @param string $term
   *   Auto-completion base term.
   *
   * @return \publicplan\wss\sodis\TypeAheadRequest
   *   Returns itself.
   */
  public function setTerm($term) {
    $access_key = urlencode(Service::getSetting(Service::SETTING__ACCESS_TOKEN));
    $url = str_replace('%KEY', $access_key, Service::getSetting(Service::SETTING__TYPEAHEAD_URI));
    $url .= '?' . $this->urlEncode(array(
      'q' => $term,
      'count' => Service::getSetting(Service::SETTING__TYPEAHEAD_COUNT),
    ));

    return $this->setUrl($url);
  }

  /**
   * Return the result set of the most recent request.
   *
   * @return array
   *   Return result set.
   */
  public function getResults() {
    return $this->results;
  }

  /**
   * Return a specific result of the most recent resultset.
   *
   * @param mixed $index
   *   Result index.
   *
   * @return mixed
   *   The desired result or FALSE, if the index does not exist.
   */
  public function getResult($index) {
    if (!array_key_exists($index, $this->results)) {
      return FALSE;
    }

    return $this->results[$index];
  }

  /**
   * Stage a request.
   *
   * @return \publicplan\wss\sodis\TypeAheadRequest
   *   Returns itself.
   * @throws \RuntimeException
   */
  public function send() {
    // Stage request and check response.
    $this->response = drupal_http_request($this->getUrl(), $this->getOptions());
    if ((int) $this->response->code !== 200) {
      throw new \RuntimeException($this->response->status_message,
          $this->response->code);
    }
    elseif (!isset($this->response->data)) {
      throw new \RuntimeException('Received response without data portion.');
    }

    // Try decoding response data and check result.
    $data = drupal_json_decode($this->response->data);
    if (!isset($data)) {
      throw new \RuntimeException('Invalid JSON in TypeAhead data portion.');
    }

    // Reformat results to fit Drupal's auto-completion conventions.
    $this->results = $this->reformatResults($data);

    return $this;
  }

  /**
   * Reformat native SODIS service response to fit Drupal's convention.
   *
   * @param array $data
   *   Input data in SODIS' native format.
   *
   * @return array
   *   Drupal auto-completion conform array.
   */
  protected function reformatResults($data) {
    $results = array();
    $max = Service::getSetting(Service::SETTING__TYPEAHEAD_COUNT);
    $hit = reset($data);
    $i = 1;
    // Iterate response data:
    while ($hit && $i < $max) {
      // Reformat data to be compatible with Drupal's built-in auto-completion
      $term = $hit['term'];
      $results[$term] = $term;
      // Prepare next loop:
      $hit = next($data);
      $i += 1;
    }

    return $results;
  }

  /**
   * Set parameters, send request and return result.
   *
   * @param string $term
   *   Search term to lookup for.
   * @param bool $use_cache
   *   Optionally disable the cache lookup.
   *
   * @return array
   *   Return the lookup results.
   */
  public static function lookup($term, $use_cache = TRUE) {
    if (!$use_cache || !($cached = cache_get($term, self::CACHE))) {
      $results = self::create()->setTerm($term)->send()->getResults();
      if ($use_cache) {
        cache_set($term, $results, self::CACHE, Service::getCacheExpirationTime());
      }
    }
    else {
      $results = $cached->data;
    }

    return $results;
  }
}

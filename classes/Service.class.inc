<?php // $id:$


/**
 * @file
 * SodisSearchService implementation.
 */

namespace publicplan\wss\sodis;

use publicplan\wss\base\SearchService;
use publicplan\lls\SimpleSAMLphpWrapper as SimpleSAML;

class Service extends SearchService {

  /**
   * Unique WSS identifier.
   */
  const NAME = 'sodis';

  /**
   * Search/Result facet: Competency.
   */
  const PARAM__COMPETENCY = 'competency';

  /**
   * Search/Result facet: Context.
   */
  const PARAM__CONTEXT = 'context';

  /**
   * Search/Result facet: Copyright.
   */
  const PARAM__COPYRIGHT = 'copyright';

  /**
   * Search/Result facet: Discipline.
   */
  const PARAM__DISCIPLINE = 'discipline';

  /**
   * Search/Result facet: Keyword.
   */
  const PARAM__KEYWORD = 'keyword';

  /**
   * Search/Result facet: Language.
   */
  const PARAM__LANGUAGE = 'language';

  /**
   * Search/Result facet: Resource type.
   */
  const PARAM__RESOURCE_TYPE = 'learningResourceType';

  /**
   * Search/Result facet: Provider.
   */
  const PARAM__PROVIDER = 'provider';

  /**
   * Search/Result facet: Publisher.
   */
  const PARAM__PUBLISHER = 'publisher';

  /**
   * Optional result offset.
   */
  const PARAM__FROM = 'from';

  /**
   * Optional result count restriction.
   */
  const PARAM__SIZE = 'size';

  /**
   * Optional result sorting.
   */
  const PARAM__SORT = 'sort';

  /**
   * Optional result sort order (defaults to self::ORDER_DESC)
   */
  const PARAM__ORDER = 'order';

  /**
   * Default sorting: By relevance.
   */
  const SORT_DEFAULT = NULL;

  /**
   * Sorting: By resource header date.
   */
  const SORT_HEADER_DATE = 'lom:header_datestamp';

  /**
   * Sorting: By publisher date.
   */
  const SORT_PUBLISHER_DATE = 'lom:publisher_date';

  /**
   * Sorting: By creator date.
   */
  const SORT_CREATOR_DATE = 'lom:creatorDate';

  /**
   * Sorting: By provider date.
   */
  const SORT_PROVIDER_DATE = 'lom:providerDate';

  /**
   * Sorting: By import date.
   */
  const SORT_IMPORT_DATE = 'importDate';

  /**
   * Sort order: Ascending.
   */
  const ORDER_ASC = 'ASC';

  /**
   * Sort order: Descending.
   */
  const ORDER_DESC = 'DESC';

  /**
   * Configuration variable names.
   */
  const SETTING__ACTIVATED = 'wss_sodis_settings__search';
  const SETTING__ACCESS_TOKEN = 'wss_sodis_settings__access_token';
  const SETTING__TYPEAHEAD_COUNT = 'wss_sodis_settings__typeahead_count';
  const SETTING__TYPEAHEAD_URI = 'wss_sodis_settings__typeahead_uri';
  const SETTING__SEARCH_URI = 'wss_sodis_settings__search_uri';
  const SETTING__RESOURCE_URI = 'wss_sodis_settings__resource_uri';
  const SETTING__RESOURCE_DOWNLOAD_URI = 'wss_sodis_settings__resource_download_uri';
  const SETTING__FACETS_URI = 'wss_sodis_settings__facets_uri';
  const SETTING__THUMBS_DIR = 'wss_sodis_settings__thumbs_dir';
  const SETTING__GET_LATEST_SORTING = 'wss_sodis_settings__get_latest_sorting';
  const SETTING__GET_LATEST_COUNT = 'wss_sodis_settings__get_latest_count';
  const SETTING__GET_LATEST_RESTRICTION = 'wss_sodis_settings__get_latest_restriction';
  const SETTING__CC_DIR = 'wss_sodis_settings__cc_dir';
  const SETTING__EDMOND_BASE_URL = 'wss_sodis_settings__edmond_base_url';
  const SETTING__EDMOND_AUTH_URL = 'wss_sodis_settings__edmond_auth_url';
  const SETTING__EDMOND_ID_PARAM = 'wss_sodis_settings__edmond_id_param';
  const SETTING__EDMOND_HANDLE_URL = 'wss_sodis_settings__edmond_handle_url';

  /**
   * Associative array containing setting identifiers and default values.
   *
   * Array keys are internally used with Drupal's variable_*() functions.
   *
   * @return array
   */
  public static function getDefaultSettings() {
    return array(
      self::SETTING__ACTIVATED => NULL,
      self::SETTING__ACCESS_TOKEN => NULL,
      self::SETTING__SEARCH_URI => NULL,
      self::SETTING__TYPEAHEAD_URI => NULL,
      self::SETTING__TYPEAHEAD_COUNT => 10,
      self::SETTING__RESOURCE_URI => NULL,
      self::SETTING__RESOURCE_DOWNLOAD_URI => NULL,
      self::SETTING__FACETS_URI => NULL,
      self::SETTING__THUMBS_DIR => '/sites/default/files/images/thumbs',
      self::SETTING__GET_LATEST_COUNT => 10,
      self::SETTING__GET_LATEST_SORTING => '',
      self::SETTING__GET_LATEST_RESTRICTION => '**',
      self::SETTING__CC_DIR => drupal_get_path('module', 'wss_sodis') . '/img',
      self::SETTING__EDMOND_BASE_URL => 'https://nrw.edupool.de/search?func=record&src=online&record=#EDMOND_ID#&standort=#STANDORT#&handle=#SESSION_ID#',
      self::SETTING__EDMOND_AUTH_URL => 'https://nrw.edupool.de/lib/auth?record=#EDMOND_ID#',
      self::SETTING__EDMOND_ID_PARAM => 'id',
      self::SETTING__EDMOND_HANDLE_URL => 'https://nrw.edupool.de/api?func=handle',
    );
  }

  /**
   * Array of possible sorting criteria.
   *
   * @return array
   *   Returns an associative array (Keys: Use with API; Values: Human readable
   *   criteria)
   */
  public static function getSortings() {
    return array(
      'lom:header_datestamp' => t('resource header date'),
      'lom:publisherDate' => t('publisher date'),
      'lom:creatorDate' => t('creator date'),
      'lom:providerDate' => t('provider date'),
      'importDate' => t('import date'),
    );
  }

  /**
   * Return the response of a SODIS facet request.
   *
   * @param $facet
   *   Facet for which to request possible values.
   *
   * @return string|FALSE
   *   The raw response body or FALSE on failure.
   *
   */
  public static function getFacetValues($facet) {
    $url = str_replace(
      array('%KEY', '%FACET'),
      array(self::getSetting(self::SETTING__ACCESS_TOKEN), $facet),
      self::getSetting(self::SETTING__FACETS_URI)
    );

    try {
      return Request::get(FALSE)->setUrl($url)->send(TRUE, FALSE);
    } catch (\Exception $e) {
      watchdog('wss_sodis', 'Failed requesting facet "%facet" (%url): %msg.', array('%facet' => $facet, '%url' => $url, '%msg' => $e->getMessage()), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  public static function getResourceUrl($id, $download = FALSE) {
  	$id = urlencode($id);
  	$url = str_replace(
      array('%KEY', '%RESOURCE'),
      array(self::getSetting(self::SETTING__ACCESS_TOKEN), $id),
      self::getSetting($download ? self::SETTING__RESOURCE_DOWNLOAD_URI : self::SETTING__RESOURCE_URI)
    );

    try {
      $response = Request::get(FALSE)->setUrl($url)->setCurlOpt(CURLOPT_NOBODY, TRUE)->send(FALSE, FALSE);
      $resp_url = !empty($response->headers['location']) ? $response->headers['location'] : $response->redirect_url;
    }
    catch (\Exception $e) {
      watchdog('wss_sodis', 'Failed requesting resource URL for ID "%id" (%url): %msg.', array('%id' => $id, '%url' => $url, '%msg' => $e->getMessage()), WATCHDOG_ERROR);
      return FALSE;
    }

    return $resp_url;
  }

  /**
   * Gets the response URL with additional EDMOND information.
   *
   * @param string $input_url
   *  The technical_location delivered by the SODIS service.
   *
   * @return string
   *  The response URL with additional EDMOND information.
   */
  public function prepareEdmondLink($input_url) {
    if (SimpleSAML::getInstance()->isAuthenticated()) {
      $edmond_base_url = self::getSetting(self::SETTING__EDMOND_AUTH_URL);
    }
    else {
      $edmond_base_url = self::getSetting(self::SETTING__EDMOND_BASE_URL);
    }

    $edmond_id_param = self::getSetting(self::SETTING__EDMOND_ID_PARAM);
    $input_query = mb_substr($input_url, mb_strpos($input_url, '?') + 1);
    $input_query_array = drupal_get_query_array($input_query);
    $lls_school_location = filter_input(INPUT_COOKIE, 'lls_school_location', FILTER_SANITIZE_NUMBER_INT);

    // URL Replacements
    $url_replacements = array(
      '#EDMOND_ID#' => isset($input_query_array[$edmond_id_param]) ? $input_query_array[$edmond_id_param] : '',
      '#STANDORT#' => $lls_school_location ? 'gemeinde='.$lls_school_location : 'standort=NRW',
    );

    // Edmond Session
    $edmond_session_handle = $this->getEdmondSessionHandle();
    if (!empty($edmond_session_handle)) {
      $url_replacements['#SESSION_ID#'] = $edmond_session_handle;
    }

    // Redirect URL
    $redirect_url = str_replace(array_keys($url_replacements), array_values($url_replacements), $edmond_base_url);

    return $redirect_url;
  }

  /**
   * Get the Edmond session handle.
   *
   * @return string
   */
  public function getEdmondSessionHandle() {
    drupal_session_start();
    if (!isset($_SESSION['lls_edmond_handle']) || empty($_SESSION['lls_edmond_handle'])) {
      $_SESSION['lls_edmond_handle'] = $this->requestEdmondSessionHandle();
    }

    return $_SESSION['lls_edmond_handle'];
  }

  /**
   * Request an edmond session handle.
   *
   * @return string Returns the session handle on success or an empty string when something went wrong.
   */
  public function requestEdmondSessionHandle() {
    $url = self::getSetting(self::SETTING__EDMOND_HANDLE_URL);
    $edmond_session_handle = '';

    try {
      $uri = @parse_url($url);
      $edmond_response = drupal_http_request($url, array(
        // If we would provide the 'Host' header as simple drupal_http_request option, the
        // chr module will override it.
        'curl_opts' => array(
          CURLOPT_HTTPHEADER => array('Host: ' . $uri['host']),
        ),
      ));
      if ($edmond_response->code !== '200') {
        throw new \Exception('Response code ' . $edmond_response->code . ' returned');
      }
      $edmond_response_json = drupal_json_decode($edmond_response->data);
      if (!isset($edmond_response_json) || !is_string($edmond_response_json['handle'])) {
        throw new \Exception('Unable to parse response body "' . print_r($edmond_response->data, TRUE) . '"');
      }
      $edmond_session_handle = $edmond_response_json['handle'];
    } catch (\Exception $e) {
      watchdog(
        'wss_sodis',
        nl2br("Error requesting edmond session handle (%loc): %msg\n%trace"),
        array(
          '%loc' => $e->getFile() . ':' . $e->getLine(),
          '%msg' => $e->getMessage(),
          '%trace' => $e->getTraceAsString(),
        ),
        WATCHDOG_WARNING
      );
    }

    return $edmond_session_handle;
  }

  /**
   * Escape search terms.
   *
   * @notice Do NOT USE for direct object requests!
   *
   * @param string $term
   *  Url encoded or plain text search term.
   *
   * @return string
   *  Url encoded and ready-to-search-escaped term.
   */
  public function escapeTerm($term) {
    $replacements = array(
      '/([^\\\]):/' => '$1\:',
    );

    $plain = urldecode($term);
    $escaped = preg_replace(array_keys($replacements), array_values($replacements), $plain);
    $encoded = urlencode($escaped);

    return $encoded;
  }
}

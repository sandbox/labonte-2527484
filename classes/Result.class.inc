<?php
// $id:$

/**
 * @file
 * This file extends the SearchResultBase class to a SODIS specific form.
 */

namespace publicplan\wss\sodis;

use publicplan\lls\SodisHelper;
use publicplan\wss\base\SearchResult;

class Result extends SearchResult {

  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service = Service::NAME;

  /**
   * Associative array representing the structure of the formatted array.
   *
   * @var array
   */
  protected $mappingStructure = array(
    'id' => '_source|lom:header_identifier',
    'title' => '_source|lom:general_title',
    'description' => '_source|lom:general_description',
    'thumbnail' => '_source|lom:relation_hasThumbnail',
    'publisher' => '_source|lom:publisher|[]',
    'provider' => '_source|lom:provider',
    'contenttype' => '_source|lom:technical_format',
    'learningResourceType' => '_source|ll3:learningResourceType|[]',
    'copyright' => '_source|lom:rights',
    'cc' => '_source|lom:rights',
    'usage' => '_source|lom:rights_de',
    'age' => '_source|lom:educational_typicalAgeRange|[]',
    'discipline' => '_source|lom:classificationDisciplineEntry|[]',
    'competency' => '_source|lom:classificationCompetencyEntry|[]',
    'context' => '_source|ll3:context|[]',
    'keyword' => '_source|lom:general_keyword|[]',
    'language' => '_source|lom:general_language|[]',
    'origin' => '_source|lom:technical_location',
    'parent' => '_source|ll3:ischildof',
    'children' => '_source|ll3:isparentof|[]',
    'siblings' => '_source|ll3:siblings|[]',
  );

  /**
   * Mapping callbacks.
   *
   * This callbacks will be applied after parsing a value using the mapping
   * structure. So it can be used to filter, translate or re-format some values.
   *
   * @var array
   */
  protected $mappingCallbacks = array(
    'thumbnail' => 'self::prepareThumb',
    'cc' => 'self::ccFilter',
    'contenttype' => 'self::mimeSubtype',
    'learningResourceType' => 'self::resourceTypeFilter',
    'language' => 'self::prepareLanguages',
    'copyright' => 'self::prepareCopyright',
    'parent' => 'self::prepareParent',
    'children' => 'self::prepareChildren',
    'siblings' => 'self::prepareChildren',
  );

  /**
   * Drop all resource object specific data.
   *
   * @return \publicplan\wss\base\SearchResult
   *   Returns itself.
   */
  public function clean() {
    parent::clean();

    return $this;
  }

  /**
   * Extract a value based on the given path from the raw data array.
   *
   * @param string $path
   *   Path to a value in the raw data array.
   *
   * @return mixed
   *   Extracted value
   */
  protected function extract($path) {
    $path_array = explode('|', $path);
    $key = reset($path_array);
    if (isset($this->raw[$key])) {
      $value = $this->raw[$key];
      while ($key = next($path_array)) {
        if ($key === '[]') {
          break;
        }
        if (is_array($value) && array_key_exists($key, $value)) {
          $value = &$value[$key];
        }
        else {
          $value = '';
        }
      }
    }

    if (is_array($value) && $key !== '[]') {
      $value = reset($value);
    }
    elseif (!is_array($value) && $key === '[]') {
      $value = empty($value) ? array() : array($value);
    }

    if (!isset($value)) {
      $value = $key === '[]' ? array() : '';
    }

    return $value;
  }

  /**
   * Parse result data.
   *
   * @param array $raw
   *   Optional raw data array. If omitted, $this->raw will be used.
   *
   * @return \publicplan\wss\sodis\Result
   *   Returns iteself.
   */
  public function parse($raw = NULL) {
    if (!isset($raw)) {
      $raw = $this->raw;
    }
    else {
      $this->raw = $raw;
    }

    // Extract data defined via $this->format_structure.
    foreach ($this->mappingStructure as $data_key => $mapping_path) {
      if (is_string($mapping_path)) {
        $data = $this->extract($mapping_path);
      }
      elseif (is_array($mapping_path) && is_callable($mapping_path)) {
        $data = call_user_func($mapping_path, $this->raw);
      }
      if (isset($this->mappingCallbacks[$data_key]) && is_callable($this->mappingCallbacks[$data_key])) {
        $this->data[$data_key . '_orig'] = $data;
        $data = call_user_func($this->mappingCallbacks[$data_key], $data);
      }
      $this->data[$data_key] = $data;
    }

    $provider = mb_strtolower($this->data['provider']);
    // Build direct link to learning resource.
    $this->data['href'] = '/sodis/resource/' . $this->data['id'] . '?provider=' . urlencode($provider);
    $this->data['href'] = check_url($this->data['href']);
    // Build link for displaying resource's meta data on SODIS.
    $this->data['href_meta'] = 'https://sodis.de/cp/ll.php?idnr=' . urlencode($this->data['id']);
    // Extend the 'origin' url by appending additional edmond params if applicable.
    if ($provider === 'edmond') {
      $this->data['origin_ext'] = Service::init()->prepareEdmondLink($this->data['id']);
    } else {
      $this->data['origin_ext'] = $this->data['origin'];
    }
    // Build link for staging a search request for the given resource ID.
    $this->data['singlesearch'] = '/suche/' . urlencode($this->data['id']);

    $this->data['rating'] = array();

    return $this;
  }

  /**
   * Return the resource location/URI as string.
   *
   * @return string
   *   Provider URL of for this result/resource.
   */
  public function getResourceLocation($resource_id = NULL) {
    $access_token = Service::getSetting(Service::SETTING__ACCESS_TOKEN);
    // Build resource download URL:
    $url = str_replace(
      array('%KEY', '%RESOURCE'),
      array($access_token, urlencode($resource_id ?: $this['id'])),
      Service::getSetting(Service::SETTING__RESOURCE_DOWNLOAD_URI)
    );
    // Stage request:
    $response = drupal_http_request($url);
    if (!isset($response->redirect_url) || empty($response->redirect_url)) {
      throw new \RuntimeException(
        t('The SODIS resource you are requesting cannot be retrieved.'), NULL,
        isset($response->error) ? new \Exception($response->error, $response->code) : NULL
      );
    }

    return $response->redirect_url;
  }

  /**
   * Return a single Result object.
   *
   * @return \publicplan\wss\sodis\Result
   *   Resource ID.
   */
  public function getResource($resource_id = NULL) {
    $access_token = Service::getSetting(Service::SETTING__ACCESS_TOKEN);
    // Build resource URL:
    $url = str_replace(
      array('%KEY', '%RESOURCE'),
      array($access_token, urlencode($resource_id ?: $this['id'])),
      Service::getSetting(Service::SETTING__RESOURCE_URI)
    );
    // Stage request:
    $response = drupal_http_request($url);
    if (!empty($response->data)) {
      $raw_data = json_decode($response->data, TRUE);
      $this->parse($raw_data);
    }

    if (!empty($this->data)) {
      return $this;
    }

    return empty($this->data) ? FALSE : $this;
  }

  /**
   * Return information about the given copyright type.
   *
   * @param string $input
   *   String to be filtered.
   *
   * @return array
   *   An array containing the following key/value pairs:
   *    - raw: the input string
   *    - lc: the lowercase version of the input string
   *    - uc: the uppercase version of the input string
   *    - href: a hyperlink to the appropriate creativecommons license
   *    - img: an url to the appropriate CC logo
   *   In cases where the input string cannot be mapped to any of the CC
   *   licenses an empty array is returned.
   */
  public static function ccFilter($input) {
    $map = array(
      'by-nc-nd' => 'http://creativecommons.org/licenses/by-nc-nd/3.0/de/',
      'by-nc' => 'http://creativecommons.org/licenses/by-nc/3.0/de/',
      'by-nc-sa' => 'http://creativecommons.org/licenses/by-nc-sa/3.0/de/',
      'by-nd' => 'http://creativecommons.org/licenses/by-nd/3.0/de/',
      'by' => 'http://creativecommons.org/licenses/by/3.0/de/',
      'by-sa' => 'http://creativecommons.org/licenses/by-sa/3.0/de/',
      'cc-zero' => 'http://creativecommons.org/about/cc0',
      'publicdomain' => 'http://creativecommons.org/about/pdm',
    );
    $match = array();
    $output = array();
    if (preg_match('/^cc[:\-](.+)$/i', $input, $match)) {
      $output['raw'] = $input;
      $output['lc'] = mb_strtolower($input);
      $output['uc'] = mb_strtoupper($input);
      $output['href'] = isset($map[$match[1]]) ? $map[$match[1]] : FALSE;
      $output['img'] = Service::getSetting(Service::SETTING__CC_DIR) . mb_strtolower($match[1]) . '.png';
    }

    return $output;
  }

  /**
   * Return filtered MIME type value (usually the subtype only).
   *
   * @param string $mime_type
   *   MIME Type string.
   *
   * @return string
   *   Subtype string.
   */
  public static function mimeSubtype($mime_type) {
    $match = array();
    if (preg_match('/^[^\/]*\/(.+)$/', $mime_type, $match)) {
      return $match[1];
    }

    return $mime_type;
  }

  /**
   * Implodes resource type array.
   *
   * @param array $resource_types
   *   Array of resource types.
   *
   * @return string
   *   Comma-seperated string of resource types.
   */
  public static function resourceTypeFilter($resource_types) {
    return implode(', ', $resource_types);
  }

  /**
   * @param mixed $languages_in
   *   Array of languages or strings with Comma-seperated list of languages.
   *
   * @return array
   *   Array of languages.
   */
  public static function prepareLanguages($languages_in) {
    $languages = array();
    $mapped_langs = array();

    foreach ($languages_in as $lang) {
      $raw_langs = explode(',', $lang);
      $langs = array_map('trim', $raw_langs);
      $languages = array_merge($languages, $langs);
    }

    foreach($languages as $lang) {
      $mapped_langs[] = SodisHelper::prepareMappingValues(Service::PARAM__LANGUAGE, $lang);
    }

    return $mapped_langs;
  }

  /**
   * @param string $str
   *   Copyright value.
   *
   * @return array
   *   Mapped copyright value.
   */
  public static function prepareCopyright($str) {
    return SodisHelper::prepareMappingValues(Service::PARAM__COPYRIGHT, $str);
  }

  /**
   * Returns the service identifier.
   *
   * @return string
   *   SODIS resource ID.
   */
  public function getServiceId() {
    return isset($this->raw['_id']) ? $this->raw['_id'] : FALSE;
  }

  /**
   * Mapping callback resolving a result records parent.
   *
   * @param string $parent
   *   Field value of 'lom:ischildof'.
   *
   * @return array|null
   *   Array containing the parsed record data.
   */
  public function prepareParent($parent) {
    $parentData = null;

    if (!empty($parent)) {
      $parentArr = explode('|:|:|:|', $parent);
      $parentData['set'] = $parentArr[0];
      $parentData['id'] = $parentArr[1];
      $parentData['title'] = $parentArr[2];
    }

    return $parentData;
  }

  /**
   * Mapping callback resolving a result records children.
   *
   * @param array $children
   *   Field value of 'lom:isparentof'.
   *
   * @return array
   *   Array of child record data arrays.
   */
  public function prepareChildren($children) {
    $childrenData = array();

    if (!empty($children)) {
      foreach ($children as $child) {
        $childArr = explode('|:|:|:|', $child);
        $childRecord['set'] = $childArr[0];
        $childRecord['id'] = $childArr[1];
        $childRecord['title'] = $childArr[2];
        $childrenData[] = $childRecord;
      }
    }

    return $childrenData;
  }
}

<?php
// $id:$

/**
 * @file
 * This file contains the SODIS specific implementation of the \SearchResponse
 * interface.
 */

namespace publicplan\wss\sodis;

use publicplan\wss\base\SearchResponse;

class Response extends SearchResponse {

  /**
   * Facet
   */
  const PARAM__COMPETENCY = 'competency';
  const PARAM__CONTEXT = 'context';
  const PARAM__COPYRIGHT = 'copyright';
  const PARAM__DISCIPLINE = 'discipline';
  const PARAM__KEYWORD = 'keyword';
  const PARAM__LANGUAGE = 'language';
  const PARAM__RESOURCE_TYPE = 'learningResourceType';
  const PARAM__PROVIDER = 'provider';
  const PARAM__PUBLISHER = 'publisher';

  const CALLBACK = 'callback';

  /**
   * Appropriate service name.
   *
   * @var string
   */
  protected $service = Service::NAME;

  protected $facets = array();

  protected $facetMapping = array(
    Service::PARAM__COMPETENCY => array(
      Service::PARAM => Service::PARAM__COMPETENCIES,
    ),
    Service::PARAM__CONTEXT => array(
      Service::PARAM => Service::PARAM__CONTEXTS,
    ),
    Service::PARAM__DISCIPLINE => array(
      Service::PARAM => Service::PARAM__DISCIPLINES,
    ),
    Service::PARAM__KEYWORD => array(
      Service::PARAM => Service::PARAM__KEYWORDS,
    ),
    Service::PARAM__LANGUAGE => array(
      Service::PARAM => Service::PARAM__LANGUAGES,
//      self::CALLBACK => '\learnline\search\classes\sodis\Response::langMapping',
    ),
    Service::PARAM__PROVIDER => array(
      Service::PARAM => Service::PARAM__PROVIDERS,
    ),
    Service::PARAM__PUBLISHER => array(
      Service::PARAM => Service::PARAM__PUBLISHERS,
    ),
    Service::PARAM__COPYRIGHT => array(
      Service::PARAM => Service::PARAM__COPYRIGHTS,
      self::CALLBACK => '\strtoupper',
//      self::CALLBACK => '\learnline\search\classes\sodis\Result::ccFilter',
    ),
    Service::PARAM__RESOURCE_TYPE => array(
      Service::PARAM => Service::PARAM__CONTENTTYPES,
    ),
  );

  /**
   * Parse common response data.
   *
   * @return \publicplan\wss\sodis\Response
   *   Returns itself.
   */
  protected function parseResponseData() {
    $this->raw = json_decode($this->body, TRUE);
    $this->data['total'] = $this->raw['hits']['total'];
    foreach ($this->raw['facets'] as $facet_name => $facet) {
      if (empty($facet['terms'])) {
        continue;
      }
      $this->data['facets'][$facet_name] = array();
      foreach ($facet['terms'] as $facet_data) {
        $this->data['facets'][$facet_name][$facet_data['term']] = $facet_data['count'];
      }
      arsort($this->data['facets'][$facet_name], SORT_NUMERIC);
    }

    return $this;
  }

  /**
   * Return array of SODIS results.
   *
   * @return array
   *   Array of SODIS result arrays (hits).
   */
  protected function extractResults() {
    if (!isset($this->raw['hits']) ||
        !isset($this->raw['hits']['hits']) ||
        !is_array($this->raw['hits']['hits'])) {
      return array();
    }

    return $this->raw['hits']['hits'];
  }
}

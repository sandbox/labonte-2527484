<?php
// $id:$

/**
 * @file
 * Module install file.
 */

// Include autoloader:
$base_module_dir = drupal_get_path('module', 'wss_base');
require_once $base_module_dir . DIRECTORY_SEPARATOR . 'wss_base.autoload.inc';

use publicplan\wss\base\SearchService;
use publicplan\wss\sodis\Service;

/**
 * Implements hook_install().
 */
function wss_sodis_install() {
  // Iterate persistent module variables:
  foreach (_wss_sodis_variables() as $name => $value) {
    // Try to read each variable:
    $var = variable_get($name, "\0");
    // Check content of the variable:
    if ($var === "\0") {
      // There seems to be no valid value in the database, so we'll store our
      // default value for this one:
      variable_set($name, $value);
    }
  }
  // Finally let's clear all cache tables:
  cache_clear_all();
}

/**
 * Implements hook_uninstall().
 */
function wss_sodis_uninstall() {
  // Check whether to keep or remove our persistent variables on deinstallation.
  // This feature can be helpful, when new persistent variables have been added
  // in a newer version of this module. In this case the module has to be
  // uninstalled (not only deactivated) to trigger the installation routine
  // on reactivation, which will insert the newly added variables to database.
  // Using the persistent settings setting, your existing settings will be
  // preserved over the whole process.
  if (!variable_get(SearchService::SETTING__PERSISTENT_SETTINGS, FALSE)) {
    // Iterate persistent variables and remove them:
    foreach (array_keys(_wss_sodis_variables()) as $name) {
      variable_del($name);
    }
  }
  // Finally clear all cache tables:
  cache_clear_all();
}

/**
 * Implements hook_schema().
 */
function wss_sodis_schema() {
  // Load drupal's default cache table schema from wss_base module:
  $wss_base_path = drupal_get_path('module', 'wss_base');
  require_once $wss_base_path . DIRECTORY_SEPARATOR . 'wss_base.install';
  // Define actual module schema:
  $module_schema = array(
    'cache_wss_sodis' => _wss_base_cache_schema('Cache SODIS requests.'),
    'cache_wss_sodis_typeahead' => _wss_base_cache_schema('Caching of SODIS type-ahead requests.'),
  );

  return $module_schema;
}

/**
 * Holds variables for (un)install routines.
 */
function _wss_sodis_variables() {
  $settings = Service::getDefaultSettings();

  return array(
    Service::SETTING__ACTIVATED => FALSE,
    Service::SETTING__TYPEAHEAD_COUNT => $settings[Service::SETTING__TYPEAHEAD_COUNT],
    Service::SETTING__ACCESS_TOKEN => 'ABC-DEF-GHI',
    Service::SETTING__TYPEAHEAD_URI => 'http://www.example.com/rest/%KEY/typeahead',
    Service::SETTING__SEARCH_URI => 'http://www.example.com/rest/%KEY/search',
    Service::SETTING__RESOURCE_URI => 'http://www.example.com/rest/%KEY/objects/%RESOURCE',
    Service::SETTING__RESOURCE_DOWNLOAD_URI => 'http://www.example.com/rest/%KEY/objects/%RESOURCE/download',
    Service::SETTING__FACETS_URI => 'http://www.example.com/rest/%KEY/facets/%FACET',
    Service::SETTING__THUMBS_DIR => $settings[Service::SETTING__THUMBS_DIR],
    Service::SETTING__GET_LATEST_SORTING => $settings[Service::SETTING__GET_LATEST_SORTING],
    Service::SETTING__GET_LATEST_COUNT => $settings[Service::SETTING__GET_LATEST_COUNT],
    Service::SETTING__GET_LATEST_RESTRICTION => $settings[Service::SETTING__GET_LATEST_RESTRICTION],
  );
}
